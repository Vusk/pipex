NAME	=	pipex

PRINT_PATH	=	./printf --no-print-directory
PRINT_NAME	=	./printf/libftprintf.a

CC		=	@clang
CFLAGS	=	-g -Wall -Wextra -Werror
RM = @rm -f

SRC	=	./src/pipex.c\
		./src/utils.c
OBJ = $(SRC:.c=.o)

all: $(PRINT_NAME) $(NAME)

$(NAME): $(OBJ)
	$(CC) $(OBJ) $(PRINT_NAME) -o $(NAME)

$(PRINT_NAME):
	@make -C $(PRINT_PATH)

clean:
	@make clean -C $(PRINT_PATH)
	$(RM) $(OBJ)

fclean : clean
	@make fclean -C $(PRINT_PATH)
	$(RM) $(NAME)

re : fclean all

.PHONY: re fclean clean all re
