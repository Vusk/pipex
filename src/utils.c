/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mrudloff <mrudloff@student.42angoulem      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/01/02 09:38:39 by mrudloff          #+#    #+#             */
/*   Updated: 2023/01/03 17:56:14 by mrudloff         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/pipex.h"

void	ft_error(const char *str)
{
	write(STDERR_FILENO, "pipex: ", 7);
	perror(str);
}

void	free_split(char **split)
{
	int	i;

	i = 0;
	while (split[i])
		i++;
	while (i >= 0)
	{
		free(split[i]);
		i--;
	}
	free(split);
}

static char	**path_tab(char *const *envp)
{
	int		i;
	char	**paths;

	i = 0;
	paths = NULL;
	while (!ft_strnstr(envp[i], "PATH", 4))
		i++;
	if (ft_strncmp(envp[i], "PATH", 4) == 0)
	{
		paths = ft_split(envp[i] + 5, ':');
		if (paths != NULL)
			return (paths);
	}
	free_split(paths);
	write(2, "Error: No path found\n", 21);
	return (NULL);
}

char	*get_path(char *cmd, char *const *envp)
{
	char	**env_path;
	char	*cmd_path;
	char	*join;
	int		i;

	if (ft_strnstr(cmd, "/", ft_strlen(cmd)))
		return (cmd);
	env_path = path_tab(envp);
	if (env_path == NULL)
		return (NULL);
	i = -1;
	while (env_path[++i])
	{
		join = ft_strjoin(env_path[i], "/");
		cmd_path = ft_strjoin(join, cmd);
		free(join);
		if (access(cmd_path, X_OK) == 0)
		{
			free_split(env_path);
			return (cmd_path);
		}
		free(cmd_path);
	}
	free_split(env_path);
	return (NULL);
}
