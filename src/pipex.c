/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pipex.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mrudloff <mrudloff@student.42angoulem      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/12/29 10:51:14 by mrudloff          #+#    #+#             */
/*   Updated: 2023/01/03 18:05:01 by mrudloff         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/pipex.h"

static void	ft_close_fd(int *fd, int *pipe)
{
	close(fd[0]);
	close(fd[1]);
	close(pipe[0]);
	close(pipe[1]);
}

static void	first_child(char **av, char *const *envp, int *pipe, int *fd)
{
	char	**cmd;
	char	*path;
	int		status;

	status = 0;
	if (dup2(fd[0], STDIN_FILENO) < 0)
		exit(EXIT_FAILURE);
	if (dup2(pipe[1], STDOUT_FILENO) < 0)
		exit(EXIT_FAILURE);
	ft_close_fd(fd, pipe);
	cmd = ft_split(av[2], ' ');
	path = get_path(cmd[0], envp);
	if (cmd && path)
		execve(path, cmd, envp);
	else
	{
		write(STDERR_FILENO, "pipex: ", 7);
		write(STDERR_FILENO, cmd[0], ft_strlen(cmd[0]));
		write(STDERR_FILENO, ": command not found\n", 20);
		status = 127;
	}
	free(path);
	free_split(cmd);
	exit(status);
}

static void	second_child(char **av, char *const *envp, int *pipe, int *fd)
{
	char	**cmd;
	char	*path;
	int		status;

	status = 0;
	if (dup2(fd[1], STDOUT_FILENO) < 0)
		exit(EXIT_FAILURE);
	if (dup2(pipe[0], STDIN_FILENO) < 0)
		exit(EXIT_FAILURE);
	ft_close_fd(fd, pipe);
	cmd = ft_split(av[3], ' ');
	path = get_path(cmd[0], envp);
	if (cmd && path)
		execve(path, cmd, envp);
	else
	{
		write(STDERR_FILENO, "pipex: ", 7);
		write(STDERR_FILENO, cmd[0], ft_strlen(cmd[0]));
		write(STDERR_FILENO, ": command not found\n", 20);
		status = 127;
	}
	free(path);
	free_split(cmd);
	exit(status);
}

static int	pipex(char **av, char *const *envp, int *fd)
{
	pid_t	pid;
	pid_t	pid2;
	int		fd_pipe[2];
	int		status;

	status = 0;
	if (pipe(fd_pipe) == -1)
		ft_error("Pipe: ");
	pid = fork();
	if (pid < 0)
		ft_error("Fork: ");
	if (pid == 0)
		first_child(av, envp, fd_pipe, fd);
	pid2 = fork();
	if (pid2 < 0)
		ft_error("Fork: ");
	if (pid2 == 0)
		second_child(av, envp, fd_pipe, fd);
	ft_close_fd(fd, fd_pipe);
	waitpid(pid, NULL, 0);
	waitpid(pid2, &status, 0);
	return (WEXITSTATUS(status));
}

int	main(int ac, char **av, char *const *envp)
{
	int	fd[2];
	int	status;

	if (ac == 5)
	{
		fd[0] = open(av[1], O_RDONLY);
		if (fd[0] < 0)
			ft_error(av[1]);
		fd[1] = open(av[4], O_CREAT | O_RDWR | O_TRUNC, 0644);
		if (fd[1] < 0)
			ft_error(av[4]);
		status = pipex(av, envp, fd);
		exit(status);
	}
	else
	{
		write(2, "Usage: ./pipex [file1] [cmd1] [cmd2] [file2]\n", 45);
		exit(22);
	}
	return (0);
}
