/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pipex.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mrudloff <mrudloff@student.42angouleme.fr  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/11/29 12:57:57 by mrudloff          #+#    #+#             */
/*   Updated: 2023/01/03 17:56:10 by mrudloff         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PIPEX_H
# define PIPEX_H

# include "../printf/include/ft_printf.h"

# include <fcntl.h>
# include <sys/wait.h>
# include <errno.h>
# include <stdio.h>

void	ft_error(const char *str);
void	free_split(char **split);
char	*get_path(char *cmd, char *const *envp);

#endif
